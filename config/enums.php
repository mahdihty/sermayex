<?php

return [
    'user' => [
        'roles' => [
            'admin' => 100,
            'member' => 200,
        ],
    ]
];