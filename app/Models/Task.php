<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'description', 'creator_id'
    ];

    public function mentions() { 
        return $this->belongsToMany(User::class);
    }

    public function creator() { 
        return $this->belongsTo(User::class, 'creator_id');
    }
}
