<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function __construct() {
        $this->authorizeResource(User::class, 'user');
    }

    public function index() {
        return UserResource::collection(User::all());
    }

    public function store(StoreUserRequest $request) {
        return UserResource::make(User::create($request->validated()));
    }

    public function show(User $user) {
        return UserResource::make($user);
    }

    public function destroy(User $user) {
        try {
            $success = $user->delete();
        } catch (\Exception $e) {
            return response()->deleteUnSuccess('user', $user->id, $e->getMessage());
        }
        
        return $success ? response()->deleteSuccess('user', $user->id) : response()->deleteUnSuccess('user', $user->id);
    }
}
