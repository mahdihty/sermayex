<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMentionRequest;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function __construct() {
        $this->authorizeResource(Task::class, 'task');
    }

    public function index() {
        return TaskResource::collection(
                Task::with(['mentions', 'creator'])
                    ->when(!auth()->user()->isAdmin(), fn($q) => $q->where('creator_id', auth()->id()))
                    ->get()
        );
    }

    public function store(StoreTaskRequest $request) {
        $taskResource = TaskResource::make($task = Task::create($request->validated())->load(['mentions', 'creator']));
        $task->mentions()->sync([auth()->id()]);
        return $taskResource;
    }

    public function show(Task $task) {
        return TaskResource::make($task->load(['mentions', 'creator']));
    }

    public function update(Task $task, UpdateTaskRequest $request) {
        $task->update($request->validated());
        return TaskResource::make($task->load(['mentions', 'creator']));
    }

    public function destroy(Task $task) {
        try {
            $success = $task->delete();
        } catch (\Exception $e) {
            return response()->deleteUnSuccess('task', $task->id, $e->getMessage());
        }
        
        return $success ? response()->deleteSuccess('task', $task->id) : response()->deleteUnSuccess('task', $task->id);
    }

    public function mention(Task $task) {
        $task->mentions()->sync([auth()->id()]);
        return TaskResource::make($task->load(['mentions', 'creator']));
    }

    /**
     * This function used in policy and map one method in this controller to one method in policy for authorization
     * 
     * @return array
     */
    public function resourceMap() {
        return [
            'mention' => 'mention'
        ];
    }
}
