<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Response as ResponseCode;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('deleteSuccess', function ($name, $id) {
            return Response::json([
                "message" => "$name with id $id deleted successfully."
            ], ResponseCode::HTTP_OK);
        });

        Response::macro('deleteUnSuccess', function ($name, $id, $err = null) {
            return Response::json([
                "message" => "Failed to delete $name with id $id.",
                "error" => env('APP_DEBUG') ? $err : null
            ], ResponseCode::HTTP_BAD_REQUEST);
        });
    }
}
