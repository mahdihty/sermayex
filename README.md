
## Installation

For install app:

- clone project.
- install composer dependency.
- run ./vendor/bin/sail up
- run ./vendor/bin/sail artisan route:list for see all routes

## Test api

For test api:

- install thunder client vs code extension.
- connect extension to thunder-tests directory.
- first call login and put token on thunder client env.
- run api.
